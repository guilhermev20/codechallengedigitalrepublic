
# Digital Republic Code Challenge
### Guilherme Veras Castagnaro Correia
Email: [guilherme.veras2000@gmail.com](mailto:guilherme.veras2000@gmail.com)  
Telefone: (11) 94563-6541  
[LinkedIn](https://www.linkedin.com/in/guilherme-veras-8143b7258/)  
[Github](https://github.com/guilhermeV20)  
Data: 11/01/2023

# Tecnologias utilizadas
- Node v16.16.0
- NestJS 9.0.0
- Docker 20.10.20

# Como executar

Este projeto pode ser executado de 2 formas:
1. Usando o Docker
2. Usando o NodeJS

## Usando o Docker

Para executar o projeto usando o Docker, é necessário ter o Docker instalado na máquina.  
Após isso, basta executar o comando abaixo na raiz do projeto:  
`docker compose up -d`  
Para parar a execução, basta executar o comando abaixo na raiz do projeto:  
`docker compose down`

## Usando o NodeJS

Para executar o projeto usando o NodeJS, é necessário ter o NodeJS instalado na máquina.
Após isso, basta executar os comandos abaixo na raiz do projeto:
1. `npm install`
2. `npm run start`

# Como utilizar

Após executar o projeto, basta acessar a URL abaixo no navegador para ver a documentação da API:  
`http://localhost:3000/api`

# Como utilizar os testes

Para executar os testes e2e, basta executar os comandos abaixo na raiz do projeto:
1. `npm install`
2. `npm run test:e2e`
