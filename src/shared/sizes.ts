export class Sizes {
  static readonly portaAltura: number = 1.9;
  static readonly portaLargura: number = 0.8;
  static readonly portaArea: number = 1.52;

  static readonly janelaAltura: number = 1.2;
  static readonly janelaLargura: number = 2;
  static readonly janelaArea: number = 2.4;
}
