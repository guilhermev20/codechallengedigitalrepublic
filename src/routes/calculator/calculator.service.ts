import { Injectable } from '@nestjs/common';
import { Sizes } from './../../shared/sizes';
import { CalculatorDto, ParedeDto } from './calculator.dto';

@Injectable()
export class CalculatorService {
  calculate(paredes: CalculatorDto) {
    function calculateParede(parede: ParedeDto): number {
      const a = parede.largura * parede.altura;
      const p = (parede.nPortas ?? 0) * Sizes.portaArea;
      const j = (parede.nJanelas ?? 0) * Sizes.janelaArea;
      return a - p - j;
    }

    const totalArea =
      calculateParede(paredes.paredeFrente) +
      calculateParede(paredes.paredeTras) +
      calculateParede(paredes.paredeDireita) +
      calculateParede(paredes.paredeEsquerda);

    let totalLitros = totalArea / 5;

    const l180 = Math.floor(totalLitros / 18);
    totalLitros -= l180 * 18;
    const l36 = Math.floor(totalLitros / 3.6);
    totalLitros -= l36 * 3.6;
    const l25 = Math.floor(totalLitros / 2.5);
    totalLitros -= l25 * 2.5;
    const l05 = Math.ceil(totalLitros / 0.5);

    return {
      '0,5l': l05,
      '2,5l': l25,
      '3,6l': l36,
      '18l': l180,
    };
  }
}
