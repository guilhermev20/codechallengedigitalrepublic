import { ApiProperty } from '@nestjs/swagger';

export class ParedeDto {
  @ApiProperty({
    description: 'Largura da parede em metros',
    example: 4,
  })
  largura: number;
  @ApiProperty({
    description: 'Altura da parede em metros',
    example: 2.5,
  })
  altura: number;

  @ApiProperty({
    description:
      'Número de portas na parede, caso contenha pelo menos uma, a altura da parede deve ser maior que a altura da porta + 30cm',
    required: false,
    example: 1,
  })
  nPortas: number;

  @ApiProperty({
    description: 'Número de janelas na parede',
    required: false,
    example: 1,
  })
  nJanelas: number;
}

export class CalculatorDto {
  @ApiProperty({
    description: 'Parede da frente',
  })
  paredeFrente: ParedeDto;
  @ApiProperty({
    description: 'Parede de trás',
  })
  paredeTras: ParedeDto;
  @ApiProperty({
    description: 'Parede da direita',
  })
  paredeDireita: ParedeDto;
  @ApiProperty({
    description: 'Parede da esquerda',
  })
  paredeEsquerda: ParedeDto;
}
