import { Body, Controller, HttpCode, Post } from '@nestjs/common';
import { BadRequestException } from '@nestjs/common/exceptions';
import { ApiOperation } from '@nestjs/swagger';
import { ApiResponse } from '@nestjs/swagger/dist';
import { Sizes } from './../../shared/sizes';
import { CalculatorDto, ParedeDto } from './calculator.dto';
import { CalculatorService } from './calculator.service';

@Controller('calculator')
export class CalculatorController {
  constructor(private calculatorService: CalculatorService) {}

  @ApiOperation({
    description: `
Endpoint para calcular a quantidade de tinta necessária para pintar as paredes de uma sala.  
A quantidade de tinta é calculada em litros, e o retorno é um objeto com a quantidade de cada tipo de lata de tinta necessária, será considerado que cada litro de tinta pode pintar 5m².  

**Latas:**
- 0,5l;
- 2,5l;
- 3,6l;
- 18l.

**Restrições:**
1. As paredes devem ter no mínimo 1m² e no máximo 50m²;  
2. O total da área das portas e janelas deve ser de no máximo 50% da área da parede;  
3. Caso uma parede possua pelo menos uma porta, a altura da parede deve ser maior que a altura da porta + 30cm;  
4. Cada janela mede 2,00x1,20 metros;  
5. Cada porta mede 0,80x1,90 metros.  
`,
  })
  @ApiResponse({
    status: 200,
    description:
      'Retorna a quantidade de cada tipo de lata de tinta necessária',
    schema: {
      type: 'object',
      properties: {
        '0.5l': {
          description: 'Quantidade de latas de 0,5l',
          type: 'number',
          example: 1,
        },
        '2.5l': {
          description: 'Quantidade de latas de 2,5l',
          type: 'number',
          example: 0,
        },
        '3.6l': {
          description: 'Quantidade de latas de 3,6l',
          type: 'number',
          example: 0,
        },
        '18l': {
          description: 'Quantidade de latas de 18l',
          type: 'number',
          example: 1,
        },
      },
    },
  })
  @ApiResponse({
    status: 400,
    description: 'Retorna um array com os erros nos dados enviados',
    schema: {
      type: 'object',
      properties: {
        statusCode: {
          type: 'number',
          example: 400,
        },
        status: {
          type: 'string',
          example: 'Bad Request',
        },
        message: {
          type: 'message',
          example: 'Body informado está incorreto ou incompleto',
        },
        errors: {
          type: 'array',
          items: {
            type: 'string',
            example: 'paredeFrente não foi informada',
          },
        },
      },
    },
  })
  @Post('')
  @HttpCode(200)
  calculate(@Body() dto: CalculatorDto) {
    const err = [];

    function validateParede(parede: ParedeDto, nomeParede: string) {
      const e = [];

      if (!parede) {
        e.push(`${nomeParede} não foi informada`);
      } else {
        if (!parede.largura) {
          e.push(`${nomeParede}.largura não foi informada`);
        } else {
          if (isNaN(parede.largura)) {
            e.push(`${nomeParede}.largura deve ser um número inteiro`);
          } else {
            if (parede.largura <= 0) {
              e.push(`${nomeParede}.largura deve ser maior que zero`);
            }
          }
        }
        if (!parede.altura) {
          e.push(`${nomeParede}.altura não foi informada`);
        } else {
          if (isNaN(parede.altura)) {
            e.push(`${nomeParede}.altura deve ser um número inteiro`);
          } else {
            if (parede.altura <= 0) {
              e.push(`${nomeParede}.altura deve ser maior que zero`);
            }
          }
        }
        if (parede.nPortas) {
          if (isNaN(parede.nPortas)) {
            e.push(`${nomeParede}.nPortas deve ser um número inteiro`);
          } else {
            if (parede.nPortas < 0) {
              e.push(`${nomeParede}.nPortas deve ser maior ou igual a zero`);
            }
          }
        }
        if (parede.nJanelas) {
          if (isNaN(parede.nJanelas)) {
            e.push(`${nomeParede}.nJanelas deve ser um número inteiro`);
          } else {
            if (parede.nJanelas < 0) {
              e.push(`${nomeParede}.nJanelas deve ser maior ou igual a zero`);
            }
          }
        }
      }
      if (e.length == 0) {
        const area = parede.largura * parede.altura;
        if (area < 1) {
          e.push(`${nomeParede} deve possuir mais de 1 metro quadrado`);
        }
        if (area > 50) {
          e.push(`${nomeParede} deve possuir menos de 50 metros quadrados`);
        }
        const usedArea =
          (parede.nPortas ?? 0) * Sizes.portaArea +
          (parede.nJanelas ?? 0) * Sizes.janelaArea;
        if (usedArea > area * 0.5) {
          e.push(
            `${nomeParede} deve possuir menos de 50% de área ocupada por portas e janelas`,
          );
        }
        if (parede.nPortas > 0 && parede.altura < Sizes.portaAltura + 0.3) {
          e.push(
            `${nomeParede} deve possuir altura de, no mínimo, 30 centímetros maior que a altura da porta`,
          );
        }
      }

      return e;
    }

    err.push(...validateParede(dto.paredeFrente, 'paredeFrente'));
    err.push(...validateParede(dto.paredeTras, 'paredeTras'));
    err.push(...validateParede(dto.paredeDireita, 'paredeDireita'));
    err.push(...validateParede(dto.paredeEsquerda, 'paredeEsquerda'));

    if (err.length > 0) {
      throw new BadRequestException({
        statusCode: 400,
        status: 'Bad Request',
        message: 'Body informado está incorreto ou incompleto',
        errors: err,
      });
    }

    return this.calculatorService.calculate(dto);
  }
}
