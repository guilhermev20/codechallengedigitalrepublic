import { Controller, Get } from '@nestjs/common';
import { ApiExcludeController } from '@nestjs/swagger';

@Controller('')
@ApiExcludeController()
export class HomeController {
  @Get('')
  home() {
    return {
      statusCode: 200,
      status: 'OK',
      message: 'Working!',
      tip: 'Try to access /api to see the API documentation',
    };
  }
}
