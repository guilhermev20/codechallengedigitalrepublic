import { Module } from '@nestjs/common';
import { CalculatorModule } from './routes/calculator/calculator.module';
import { HomeModule } from './routes/home/home.module';

@Module({
  imports: [HomeModule, CalculatorModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
