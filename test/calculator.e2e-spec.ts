import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('Calculator 200 /calculator (POST)', () => {
    return request(app.getHttpServer())
      .post('/calculator')
      .send({
        paredeFrente: {
          largura: 4,
          altura: 2.5,
          nPortas: 1,
        },
        paredeTras: {
          largura: 4,
          altura: 2,
        },
        paredeDireita: {
          largura: 5,
          altura: 2,
          nJanelas: 1,
        },
        paredeEsquerda: {
          largura: 5,
          altura: 2,
        },
      })
      .expect(200)
      .expect(
        JSON.stringify({
          '0,5l': 2,
          '2,5l': 1,
          '3,6l': 1,
          '18l': 0,
        }),
      );
  });

  it('Calculator 400 no data /calculator (POST)', () => {
    return request(app.getHttpServer()).post('/calculator').expect(400);
  });

  it('Calculator 400 missing data /calculator (POST)', () => {
    return request(app.getHttpServer())
      .post('/calculator')
      .send({
        paredeFrente: {
          largura: 4,
          altura: 2.5,
          nPortas: -1,
        },
        paredeTras: {
          largura: 4,
          altura: 2,
        },
        paredeEsquerda: {
          largura: 5,
        },
      })
      .expect(400)
      .expect(
        JSON.stringify({
          statusCode: 400,
          status: 'Bad Request',
          message: 'Body informado está incorreto ou incompleto',
          errors: [
            'paredeFrente.nPortas deve ser maior ou igual a zero',
            'paredeDireita não foi informada',
            'paredeEsquerda.altura não foi informada',
          ],
        }),
      );
  });
});
