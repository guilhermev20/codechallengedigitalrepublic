# syntax=docker/dockerfile:1
FROM node:16-alpine AS development

WORKDIR /app

COPY ["package.json", "./"]

RUN npm i

COPY . .

RUN npm run build

FROM node:16-alpine AS production

WORKDIR /app

COPY ["package.json", "./"]

RUN npm i --omit=dev

COPY --from=development /app/dist ./dist

CMD ["node", "dist/main"]